﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Threading;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FFXIVClassic_REST_Server {
	class WebServer {
		HttpListener httpListener;
		Thread thread;
		String rootPath;

        static string createUserPage = "/account/content/createUser.html";
		static string loginPage = "/account/content/classiclogin.html"; // you can change this line to /account/content/classiclogin.html if you want classic login by default or set the modern login page as default by changing the line to /account/content/login.html
        static string startGamePage = "/account/content/startGame.html";

        static string GAME_NAME = "ffxiv";
        static string GAME_HASH = "48eca647";
        static string BOOT_HASH = "2d2a390f";

        public WebServer(String path) {

			rootPath = path;

			httpListener = new HttpListener();

            //Serve on the launch server port
			httpListener.Prefixes.Add($"http://+:{ConfigConstants.OPTIONS_PORT}/");
            Program.Log.Info($"Serving on http://+:{ConfigConstants.OPTIONS_PORT}/");

            //Server on the patch server port
            httpListener.Prefixes.Add($"http://+:{ConfigConstants.PATCHER_PORT}/");
            Program.Log.Info($"Serving on http://+:{ConfigConstants.PATCHER_PORT}/");

            //Server on the torrent server port
            httpListener.Prefixes.Add($"http://+:8999/");
            Program.Log.Info($"Serving on http://+:8999/");
            httpListener.Prefixes.Add($"http://+:54997/");
            Program.Log.Info($"Serving on http://+:54997/");


            httpListener.Start();

			thread = new Thread(this.Listen);
			thread.Start();
		}
		private void Listen() {
			while(httpListener.IsListening) {
				try {
					HttpListenerContext context = httpListener.GetContext();
					Process(context);
					Thread.Yield();

				} catch(Exception ex) {
					Program.Log.Info(ex);
				}
			}
		}

		private void Process(HttpListenerContext context) {

			switch(context.Request.HttpMethod) {
				case "GET":
					context = GET(context);
					break;

				case "POST":
					context = POST(context);
					break;

				case "OPTIONS":
					context.Response.AddHeader("Allow", "GET,POST,OPTIONS");
					break;
				default:
					context.Response.StatusCode = 405;
					break;
			}
			context.Response.OutputStream.Close();
		}

		private HttpListenerContext GET(HttpListenerContext context) {
            Program.Log.Info("GETing " + context.Request.Url.LocalPath.Substring(1));
            List<string> segments = new List<string>(context.Request.Url.Segments);

            /* debugging code. Nothing to see here
            Program.Log.Info(segments.Count + " segments found");
            foreach (String s in segments)
            {
                Program.Log.Info(s);
            }
            */
            if (segments.Count == 1 && segments.ElementAt(1) == "lobby/") {
				context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

			}
            else if (!context.Request.Url.LocalPath.Substring(1).Contains(".htm") &&
                     segments.Count == 8 &&
                     segments.ElementAt(1) == "patch/" &&
                     segments.ElementAt(2) == "vercheck/" &&
                     segments.ElementAt(3) == "ffxiv/" &&
                     segments.ElementAt(4) == "win32/" &&
                     segments.ElementAt(5) == "release/")
            {
                if (segments.ElementAt(6) == "boot/")
                {
                    checkBootVersion(context, segments.ElementAt(7));
                }
                else if (segments.ElementAt(6) == "game/")
                {
                    checkGameVersion(context, segments.ElementAt(7));
                }
                else
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Program.Log.Info(context.Request.Url.LocalPath.Substring(1) + " patch not found");
                }
            }
            else if (context.Request.Url.LocalPath.Substring(1) == $"account/content/ffxivlogin")
            {
                context.Response.Redirect($"{loginPage}");
            }
            else {
				String filePath = Path.Combine(rootPath, context.Request.Url.LocalPath.Substring(1));
				if(File.Exists(filePath)) {
					try {
						String mimeType = MimeTypeLookup.GetMimeType(filePath);
						String dateTime = DateTime.Now.ToString("r");
						String lastMod = System.IO.File.GetLastWriteTime(filePath).ToString("r");
						FileStream input = System.IO.File.OpenRead(filePath);

						context.Response.ContentType = mimeType;
						context.Response.ContentLength64 = input.Length;
						context.Response.AddHeader("Date", dateTime);
						context.Response.AddHeader("Last-Modified", lastMod);

						input.CopyTo(context.Response.OutputStream);
						input.Close();

						context.Response.StatusCode = (int) HttpStatusCode.OK;

					} catch(Exception ex) {
						Program.Log.Error(ex);
						context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
					}
				} else {
					context.Response.StatusCode = (int) HttpStatusCode.NotFound;
					Program.Log.Info(Path.GetFileName(filePath) + " not found.");
				}
			}
			return context;
		}

        private void checkBootVersion(HttpListenerContext context, string version)
        {
            Program.Log.Info("Checking boot version");
            if (version != ConfigConstants.BOOT_VERSION)
			{
                Program.Log.Info("Boot version " + version + " didn't match. Patch your shit.");
                //TODO: replace with version number lookup for stuff later
                String newpatchversion; 
                
                if (!PatchList.bootpatches.TryGetValue(version, out newpatchversion))
                {
                    Program.Log.Info("Invalid boot patch requested: " + version);
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    //context.Response.AddHeader("HTTP/1.0 404 NOT FOUND");
                    return;
                }

                context.Response.StatusCode = (int)HttpStatusCode.OK;
                //context.Response.AddHeader("HTTP/1.0 200 OK");
                context.Response.AddHeader("Content-Location", $"{GAME_NAME}/{BOOT_HASH}/vercheck.dat");
                context.Response.AddHeader("Content-Type", "multipart/mixed; boundary=477D80B1_38BC_41d4_8B48_5273ADB89CAC");
                context.Response.AddHeader("X-Repository", $"{GAME_NAME}/win32/release/boot");
                context.Response.AddHeader("X-Patch-Module", "ZiPatch");
                context.Response.AddHeader("X-Protocol", "torrent");
                context.Response.AddHeader("X-Info-Url", "http://example.com");
                context.Response.AddHeader("X-Latest-Version", ConfigConstants.BOOT_VERSION);
                context.Response.AddHeader("Connection", "keep-alive");

                sendBootPatches(version, ConfigConstants.BOOT_VERSION, context);
            }
			else
			{
                Program.Log.Info("Boot version matched. Proceed to game version check");
                context.Response.StatusCode = (int)HttpStatusCode.NoContent;
                //context.Response.AddHeader("HTTP/1.0 204 No Content");
                context.Response.AddHeader("Content-Location", $"{GAME_NAME}/{BOOT_HASH}/vercheck.dat");
                context.Response.AddHeader("X-Repository", $"{GAME_NAME}/win32/release/boot");
                context.Response.AddHeader("X-Patch-Module", "ZiPatch");
                context.Response.AddHeader("X-Protocol", "torrent");
                context.Response.AddHeader("X-Info-Url", "http://www.example.com");
                context.Response.AddHeader("X-Latest-Version", ConfigConstants.BOOT_VERSION);
            }

        }

        private void sendBootPatches(string startversion, string finalpatchversion, HttpListenerContext context)
        {
            

            //StringBuilder sb = new StringBuilder();
            StreamWriter sb = new StreamWriter(context.Response.OutputStream);
            BinaryWriter bw = new BinaryWriter(context.Response.OutputStream);
            string nextpatchversion = startversion;

            while (true)
            {

                if (!PatchList.bootpatches.TryGetValue(nextpatchversion, out nextpatchversion))
                    break;

                string filepath = "";

                if (File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/metainfo/D{nextpatchversion}.torrent")) &&
                    File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/patch/D{nextpatchversion}.patch")))
                {
                    filepath = $"D{nextpatchversion}";
                }
                else if (File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/metainfo/H{nextpatchversion}.torrent")) &&
                    File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/patch/H{nextpatchversion}.patch")))
                {
                    filepath = $"H{nextpatchversion}";
                }

                Program.Log.Info("Sending patch info for " + filepath);

                sb.Write("--477D80B1_38BC_41d4_8B48_5273ADB89CAC\r\n");
                
                sb.Write("Content-Type: application/octet-stream\r\n");
                sb.Write($"Content-Location: {GAME_NAME}/{BOOT_HASH}/metainfo/{filepath}.torrent\r\n");
                sb.Write($"X-Patch-Length: {new FileInfo($"{rootPath}/{GAME_NAME}/{BOOT_HASH}/metainfo/{filepath}.torrent").Length}\r\n");
                sb.Write("X-Signature: jqxmt9WQH1aXptNju6CmCdztFdaKbyOAVjdGw_DJvRiBJhnQL6UlDUcqxg2DeiIKhVzkjUm3hFXOVUFjygxCoPUmCwnbCaryNqVk_oTk_aZE4HGWNOEcAdBwf0Gb2SzwAtk69zs_5dLAtZ0mPpMuxWJiaNSvWjEmQ925BFwd7Vk=\r\n");

                sb.Write("\r\n");
                sb.Flush();
                //sb.Write(File.ReadAllBytes(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/metainfo/{filepath}.torrent")));
                
                bw.Write(File.ReadAllBytes(Path.Combine(rootPath, $"{GAME_NAME}/{BOOT_HASH}/metainfo/{filepath}.torrent")));
                bw.Flush();
                

                sb.Write("\r\n");

                if (nextpatchversion == finalpatchversion)
                    break;
                
            }

            

            sb.Write("--477D80B1_38BC_41d4_8B48_5273ADB89CAC--\r\n\r\n");

            sb.Flush();
            bw.Close();
            bw.Dispose();
            sb.Close();
            sb.Dispose();
        }

        private void checkGameVersion(HttpListenerContext context, string version)
        {
            Program.Log.Info("Checking game version");
            if (version != ConfigConstants.GAME_VERSION)
            {
                Program.Log.Info("Game version didn't match. Patch your shit.");
                //TODO: replace with version number lookup for stuff later
                if (version == "")
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    //context.Response.AddHeader("HTTP/1.0 404 NOT FOUND");
                    return;
                }

                context.Response.StatusCode = (int)HttpStatusCode.OK;
                //context.Response.AddHeader("HTTP/1.0 200 OK");
                context.Response.AddHeader("Content-Location", $"{GAME_NAME}/{GAME_HASH}/vercheck.dat");
                context.Response.AddHeader("Content-Type", "multipart/mixed; boundary=477D80B1_38BC_41d4_8B48_5273ADB89CAC");
                context.Response.AddHeader("X-Repository", $"{GAME_NAME}/win32/release/game");
                context.Response.AddHeader("X-Patch-Module", "ZiPatch");
                context.Response.AddHeader("X-Protocol", "torrent");
                context.Response.AddHeader("X-Info-Url", "http://example.com");
                context.Response.AddHeader("X-Latest-Version", ConfigConstants.GAME_VERSION);
                context.Response.AddHeader("Connection", "keep-alive");

                sendGamePatches(version, ConfigConstants.GAME_VERSION, context);
            }
            else
            {
                Program.Log.Info("Game version matched. Proceed to login");
                context.Response.StatusCode = (int)HttpStatusCode.NoContent;
                //context.Response.AddHeader("HTTP/1.0 204 No Content");
                context.Response.AddHeader("Content-Location", $"{GAME_NAME}/{GAME_HASH}/vercheck.dat");
                context.Response.AddHeader("X-Repository", $"{GAME_NAME}/win32/release/game");
                context.Response.AddHeader("X-Patch-Module", "ZiPatch");
                context.Response.AddHeader("X-Protocol", "torrent");
                context.Response.AddHeader("X-Info-Url", "http://www.example.com");
                context.Response.AddHeader("X-Latest-Version", ConfigConstants.GAME_VERSION);
            }
        }

        private void sendGamePatches(string startversion, string finalpatchversion, HttpListenerContext context)
        {
            

            //StringBuilder sb = new StringBuilder();
            StreamWriter sb = new StreamWriter(context.Response.OutputStream);
            BinaryWriter bw = new BinaryWriter(context.Response.OutputStream);
            string nextpatchversion = startversion;

            while (true)
            {

                if (!PatchList.gamepatches.TryGetValue(nextpatchversion, out nextpatchversion))
                    break;

                string filepath = "";

                if (File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/metainfo/D{nextpatchversion}.torrent")) 
                    //&& File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/patch/D{nextpatchversion}.patch"))
                    )
                {
                    filepath = $"D{nextpatchversion}";
                }
                else if (File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/metainfo/H{nextpatchversion}.torrent")) 
                    //&& File.Exists(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/patch/H{nextpatchversion}.patch"))
                    )
                {
                    filepath = $"H{nextpatchversion}";
                }

                Program.Log.Info("Sending patch info for " + filepath);

                sb.Write("--477D80B1_38BC_41d4_8B48_5273ADB89CAC\r\n");

                sb.Write("Content-Type: application/octet-stream\r\n");
                sb.Write($"Content-Location: {GAME_NAME}/{GAME_HASH}/metainfo/{filepath}.torrent\r\n");
                sb.Write($"X-Patch-Length: {new FileInfo($"{rootPath}/{GAME_NAME}/{GAME_HASH}/metainfo/{filepath}.torrent").Length}\r\n");
                sb.Write("X-Signature: jqxmt9WQH1aXptNju6CmCdztFdaKbyOAVjdGw_DJvRiBJhnQL6UlDUcqxg2DeiIKhVzkjUm3hFXOVUFjygxCoPUmCwnbCaryNqVk_oTk_aZE4HGWNOEcAdBwf0Gb2SzwAtk69zs_5dLAtZ0mPpMuxWJiaNSvWjEmQ925BFwd7Vk=\r\n");

                sb.Write("\r\n");
                sb.Flush();
                //sb.Write(File.ReadAllBytes(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/metainfo/{filepath}.torrent")));

                bw.Write(File.ReadAllBytes(Path.Combine(rootPath, $"{GAME_NAME}/{GAME_HASH}/metainfo/{filepath}.torrent")));
                bw.Flush();


                sb.Write("\r\n");

                if (nextpatchversion == finalpatchversion)
                    break;

            }



            sb.Write("--477D80B1_38BC_41d4_8B48_5273ADB89CAC--\r\n\r\n");

            sb.Flush();
            bw.Close();
            bw.Dispose();
            sb.Close();
            sb.Dispose();
        }

        private HttpListenerContext POST(HttpListenerContext context) {
            Program.Log.Info("POSTing " + context.Request.Url.LocalPath.Substring(1));
            List<string> segments = new List<string>(context.Request.Url.Segments);
            // account/content/ffxivlogin are the first 3 segments
            /* debugging code. Nothing to see here
            Program.Log.Info(segments.Count + " segments found");
            foreach (String s in segments)
            {
                Program.Log.Info(s);
            }
            */

            if (segments.Count != 3) {
				return context;
			}
			if(segments.ElementAt(1) == "lobby/") {
				if(segments.ElementAt(2) == "createaccount") {
					return CreateAccount(context);
				}
				if(segments.ElementAt(2) == "login") {
					return LoginAccount(context);
				}
			}
			return context;
		}

		private HttpListenerContext CreateAccount(HttpListenerContext context) {
			if(context.Request.ContentType.ToLower() != "application/x-www-form-urlencoded") {
				return context;
			}
			string formData = new StreamReader(context.Request.InputStream).ReadToEnd();
			NameValueCollection formQuery = HttpUtility.ParseQueryString(formData);
			NameValueCollection newQuery = HttpUtility.ParseQueryString(formQuery.ToString());
			newQuery.Remove("password");
			newQuery.Remove("verifypassword");
            String redirectPage = createUserPage + "?" + newQuery.ToString();

			String lang, region, username, password, verifypassword, email;
			if((lang = formQuery["lang"]) == null || lang.Length == 0) {
				context.Response.Redirect(redirectPage + "#langError");
				return context;
			}
			if((region = formQuery["region"]) == null || region.Length == 0) {
				context.Response.Redirect(redirectPage + "#regionError");
				return context;
			}
			if((username = formQuery["username"]) == null || username.Length == 0 || !Regex.IsMatch(username, "^[a-zA-Z0-9]+$")) {
				context.Response.Redirect(redirectPage + "#usernameError");
				return context;
			}
			if((password = formQuery["password"]) == null || password.Length == 0) {
				context.Response.Redirect(redirectPage + "#passwordError");
				return context;
			}
			if((verifypassword = formQuery["verifypassword"]) == null || verifypassword.Length == 0 || (verifypassword != password)) {
				context.Response.Redirect(redirectPage + "#verifyPasswordError");
				return context;
			}
			if((email = formQuery["email"]) == null || email.Length == 0 || !(new EmailAddressAttribute().IsValid(email))) {
				context.Response.Redirect(redirectPage + "#emailError");
				return context;
			}

			if(SqlServer.DoesUsernameExist(username)) {
				Program.Log.Info("Username " + username + " exists.");
				context.Response.Redirect(redirectPage + "#usernameExistError");
				return context;
			}

			if(SqlServer.CreateAccount(username, password, email, lang, region)) {
				context.Response.Redirect(loginPage + "#createSuccess");
			}
			return context;
		}

		private HttpListenerContext LoginAccount(HttpListenerContext context) {
			if(context.Request.ContentType.ToLower() != "application/x-www-form-urlencoded") {
				return context;
			}
			String formData = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();
			NameValueCollection formQuery = HttpUtility.ParseQueryString(formData);

			NameValueCollection newQuery = HttpUtility.ParseQueryString(formQuery.ToString());
			newQuery.Remove("password");
			String redirectPage = loginPage + "?" + newQuery.ToString();

			Program.Log.Info(newQuery.ToString());

			String username, password;
			if((username = formQuery["username"]) == null || username.Length == 0 || !Regex.IsMatch(username, "^[a-zA-Z0-9]+$")) {
                Program.Log.Info($"Invalid username: {username}");
				context.Response.Redirect($"{redirectPage}#usernameError");
				return context;
			}
			if((password = formQuery["password"]) == null || password.Length == 0) {
                Program.Log.Info($"Invalid password for: {username}");
                context.Response.Redirect($"{redirectPage}#passwordError");
				return context;
			}

			int uid = -1;
			if((uid = SqlServer.LoginAccount(username, password)) != -1) {
				Program.Log.Info(String.Format("{0} ({1}) logged in.", username, uid));

                string sid = SqlServer.CreateOrRefreshSession(uid);
                if ((newQuery["session"] = sid).Length > 0) {
                    // TODO respond with lang/region
                    String successUrl = $"{startGamePage}?{newQuery.ToString()}";
					context.Response.Redirect(successUrl);
                    
                    /* if for some reason javascript dies, we can always just output the html directly.
                    StreamWriter sb = new StreamWriter(context.Response.OutputStream);
                    sb.Write($"<x-sqexauth sid=\"{sid}\" lang=\"en-us\" region=\"2\" utc=\"{DateTime.UtcNow}\" />");
                    sb.Flush();
                    sb.Dispose();
                    */

                    return context;
                }

			} else {
				Program.Log.Info(String.Format("{0} ({1}) failed login.", username, uid));
				context.Response.Redirect($"{redirectPage}#loginFailedError");
			}

			return context;

			// Boot with 7U Launcher = "ffxiv://login_success?sessionId="+parsed.sid;
			// Boot with official launcher
			// document.getElementById("Error").innerHTML = "<x-sqexauth sid=\""+parsed.sid+"\" lang=\""+parsed.lang+"\" region=\""+parsed.region+"\" utc=\""+parsed.utc+"\" />";
		}
		
		public bool IsListening() {
			return httpListener.IsListening;
		}
	}
}
