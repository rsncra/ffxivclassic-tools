﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HashLib;
using MySql.Data.MySqlClient;

namespace FFXIVClassic_REST_Server {
	class SqlServer {
		public static bool TestConnection() {
			Program.Log.Info("Testing DB connection to \"{0}\"... ", ConfigConstants.DATABASE_HOST);

			String query = String.Format("Server={0}; Port={1}; Database={2}; UID={3}; Password={4}",
				ConfigConstants.DATABASE_HOST, ConfigConstants.DATABASE_PORT, ConfigConstants.DATABASE_NAME,
				ConfigConstants.DATABASE_USERNAME, ConfigConstants.DATABASE_PASSWORD);

			using(MySqlConnection conn = new MySqlConnection(query)) {
				try {
					conn.Open();
					conn.Close();

					Program.Log.Info("DB connection ok.");
					return true;
				} catch(MySqlException e) {
					Program.Log.Error(e.ToString());
					return false;
				}
			}
		}
		private static MySqlConnection Connect() {
			MySqlConnection con = null;
			try {
				con = new MySqlConnection(String.Format("Server={0}; Port={1}; Database={2}; UID={3}; Password={4}",
					ConfigConstants.DATABASE_HOST, ConfigConstants.DATABASE_PORT, ConfigConstants.DATABASE_NAME,
					ConfigConstants.DATABASE_USERNAME, ConfigConstants.DATABASE_PASSWORD));
				return con;

			} catch(MySqlException e) {
				Program.Log.Error(e.ToString());
				if(con != null) {
					con.Close();
				}
				return null;
			}
		}
		public static bool DoesUsernameExist(String username) {
			using(MySqlConnection con = Connect()) {
				con.Open();

				MySqlCommand cmd = new MySqlCommand("SELECT name FROM users WHERE name = @username", con);
				cmd.Prepare();

				cmd.Parameters.AddWithValue("@username", username);
				using(MySqlDataReader reader = cmd.ExecuteReader()) {
					if(reader.HasRows) {
						return true;
					}
				}
				return false;
			}
		}

		public static bool CreateAccount(String username, String password, String email, String lang, String region) {

			// Taken from SimpleHTTPServer ----

			//generate the salt and password hash
			int random = 0;
			using(RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider()) {
				byte[] rno = new byte[5];
				rng.GetBytes(rno);
				random = BitConverter.ToInt32(rno, 0);
			}

			//hash that random number to generate our salt
			IHash hasher = HashFactory.Crypto.CreateSHA224();
			HashResult hr = hasher.ComputeInt(random);
			String salt = hr.ToString().Replace("-", "").ToLower();

			//fix up hashed password's formatting to match expected formatting
			HashResult r = hasher.ComputeString((password + salt), Encoding.ASCII);
			String hashedpassword = r.ToString().Replace("-", "").ToLower();

            //format a proper language launch setting based on lang
            if (lang.Equals("EN"))
            {
                lang = "en-us";
            }
            else if (lang.Equals("UK"))
            {
                lang = "en-gb";
            }
            else if (lang.Equals("DE"))
            {
                lang = "de-de";
            }
            else if (lang.Equals("FR"))
            {
                lang = "fr-fr";
            }
            else if (lang.Equals("JA"))
            {
                lang = "ja-jp";
            }

			// ---

			using(MySqlConnection con = Connect()) {
				try {
					con.Open();

					MySqlCommand cmd = new MySqlCommand("INSERT INTO users (name, passhash, salt, email, lang, region)" +
						"VALUES (@username, @passhash, @salt, @email, @lang, @region)", con);
					cmd.Prepare();

					cmd.Parameters.AddWithValue("@username", username);
					cmd.Parameters.AddWithValue("@passhash", hashedpassword);
					cmd.Parameters.AddWithValue("@salt", salt);
					cmd.Parameters.AddWithValue("@email", email);
					cmd.Parameters.AddWithValue("@lang", lang);
					cmd.Parameters.AddWithValue("@region", region);

					int affectedrows = cmd.ExecuteNonQuery();
					if(affectedrows == 1) {
						return true;
					}
					return false;

				} catch(MySqlException e) {
					Program.Log.Error(e.ToString());
					return false;
				}
			}
		}
		public static int LoginAccount(String username, String password) {
			using(MySqlConnection con = Connect()) {
				try {
					con.Open();
					MySqlCommand cmd = new MySqlCommand("SELECT id, passhash, salt, lang, region FROM users WHERE name = @username", con);
					cmd.Prepare();

					cmd.Parameters.AddWithValue("@username", username);

					using(MySqlDataReader reader = cmd.ExecuteReader()) {
						if(reader.HasRows) {
							reader.Read();

							//check the password
							String storedpasshash = reader.GetString(1);
							String salt = reader.GetString(2);

							String saltedpassword = password + salt;
							IHash hasher = HashFactory.Crypto.CreateSHA224();

							//fix up hashed password's formatting to match expected formatting
							HashResult r = hasher.ComputeString(saltedpassword, Encoding.ASCII);
							String hashedpassword = r.ToString().Replace("-", "").ToLower();

							if(storedpasshash.Equals(hashedpassword)) {
								return reader.GetInt32(0);

							}
						}
					}
					return -1;

				} catch(MySqlException e) {
					Program.Log.Error(e.ToString());
					return -1;
				}
			}
		}
		public static String CreateOrRefreshSession(int uid) {
			using(MySqlConnection con = Connect()) {
				try {
					con.Open();
					String sid = GetSessionID(con, uid);
					if(sid != null) {
						RefreshSession(con, sid);
					} else {
						sid = CreateSessionID(con, uid);
					}
					return sid;
				} catch(MySqlException e) {
					Program.Log.Error(e.ToString());
					return null;
				}
			}
		}
		public static String GetSessionID(MySqlConnection con, int uid) {
			MySqlCommand cmd = new MySqlCommand("SELECT id FROM sessions WHERE userId = @uid AND expiration > NOW()", con);
			cmd.Prepare();

			cmd.Parameters.AddWithValue("@uid", uid);

			using(MySqlDataReader reader = cmd.ExecuteReader()) {
				if(reader.HasRows) {
					reader.Read();
					return reader.GetString(0);
				}
			}
			return null;
		}
		public static String CreateSessionID(MySqlConnection con, int uid) {

			//make a nice random number
			int random = 0;
			using(RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider()) {
				byte[] rno = new byte[5];
				rng.GetBytes(rno);
				random = BitConverter.ToInt32(rno, 0);
			}

			//hash that random number to generate our sessionid
			IHash hasher = HashFactory.Crypto.CreateSHA224();
			HashResult r = hasher.ComputeInt(random);
			String generatedsessionid = r.ToString().Replace("-", "").ToLower();


			//delete any sessions associated with this uid
			//Program.Log.Info("Deleting any expired sessions");
			MySqlCommand cmd = new MySqlCommand("DELETE FROM sessions WHERE userId = @uid", con);
			cmd.Prepare();

			cmd.Parameters.AddWithValue("@uid", uid);
			cmd.ExecuteNonQuery();

			//actually create a new session
			//Program.Log.Info("Creating a new session");
			MySqlCommand cmd2 = new MySqlCommand(
				String.Format("INSERT INTO sessions (id, userid, expiration) VALUES (@sessionID, @uid, NOW() + INTERVAL {0} HOUR)", ConfigConstants.FFXIV_SESSION_LENGTH),
				con);
			cmd2.Prepare();

			cmd2.Parameters.AddWithValue("@sessionID", generatedsessionid);
			cmd2.Parameters.AddWithValue("@uid", uid);

			cmd2.ExecuteNonQuery();

			return generatedsessionid;
		}
		public static bool RefreshSession(MySqlConnection con, string sid) {
			if(sid == null || sid.Length == 0) {
				return false;
			}
			MySqlCommand cmd = new MySqlCommand(
				String.Format("UPDATE sessions SET expiration = NOW() + INTERVAL {0} HOUR WHERE id = @id", ConfigConstants.FFXIV_SESSION_LENGTH),
				con);
			cmd.Prepare();

			cmd.Parameters.AddWithValue("@id", sid);

			int sessionrowsaffected = cmd.ExecuteNonQuery();
			if(sessionrowsaffected > 0) {
				return true;
			}
			return false;
		}
	}
}
