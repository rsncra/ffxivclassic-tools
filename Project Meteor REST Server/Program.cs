﻿using System;
using System.Diagnostics;
using System.Threading;
using NLog;
using System.IO;

namespace FFXIVClassic_REST_Server
{
    class Program
    {
        public static Logger Log;

		static void Main(string[] args)
        {

            // set up logging
            Log = LogManager.GetCurrentClassLogger();
#if DEBUG
            TextWriterTraceListener myWriter = new TextWriterTraceListener(System.Console.Out);
            Debug.Listeners.Add(myWriter);
#endif
            Log.Info("==================================");
            Log.Info("Project Meteor REST Web Server");
            Log.Info("Version: 0.1");
            Log.Info("==================================");

            //Load Config
            ConfigConstants.Load();
            ConfigConstants.ApplyLaunchArgs(args);
            PatchList.Load();


			// Test DB Connection
			if(SqlServer.TestConnection()) {
				String webRoot = Path.Combine(Directory.GetCurrentDirectory(), "web");
				WebServer server = new WebServer(webRoot);
				while(server.IsListening()) {
					Thread.Yield();
				}
			} else {
				Program.Log.Info("Cannot connect to DB.");
			}

            Program.Log.Info("Press any key to continue...");
            Console.ReadKey();
        }


    }
}
