Project Meteor REST Web server
This tool allows you to set up a web server for Project Meteor ( Which is an emulator of Final Fantasy XIV 1.0) to allow the official launcher to connect to a locally hosted instansce of project meteor
Follow the setup guide here untill the wamp portion of it http://ffxivclassic.fragmenterworks.com/wiki/index.php/Setting_up_the_project then compile the hosted here web server and set up mariadb

Afterwards edit the hosts file by adding those lines into it

// This is where the game will look for the lobby server

127.0.0.1    lobby01.ffxiv.com

// These are part of login/patching/version checking

127.0.0.1    patch01.ffxiv.com

127.0.0.1    track01.ffxiv.com

127.0.0.1    ver01.ffxiv.com

127.0.0.1    account.square-enix.com

Also edit the users table in database by including the following columns:

lang = char(2) - make the default "EN"

region = int = make the default 2
